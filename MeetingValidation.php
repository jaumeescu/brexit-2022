<?php
use \Phppot\Member;
require_once ("class/Member.php");


class MeetingValidation
{

    public $specialUsers = array(
    'PRODECA'=>'PRODECA',
    );

    public $member;


    
    
    //public $eventCode = 'demo2';
    //public $secretWord = "demoword";
   // public $mainURL = "https://mydemo.eventszone.net/demo_myCongress/services/";

	public $eventCode = 'm20';
    public $secretWord = "LLPPD1234Dee";
	//public $mainURL = "https://sepa.eventszone.net/myCongress/services/";

    public $cookiename = "meetngs";
    public $cookieUsername = "meetngsU";
    public $username;
    public $password;
    public $hashKey;
    public $url;
    public $format = 'json';
    public $id;
    public $user;
    public $codi_sessio;

    public $eventPERIO = 'm20';
    public $cuotasPERIO = array('8','1SIMP1','1SIMP2','249SEPA','1SIMP2','31ENERO','afw','agent','AGON','aneo','blackfriday','cddv','COLOM','CONGRESISTASEPA','dafavad','dcddc','DFGT54','DFSDA','dsfsd','esp249','EXPOP','FULLCON','g1','g13','g2','g38','g44','g47','gatu','infecte','INVOR','INVPL','KLOCKNR','LATAM','maxillaris','ORABLGR','PEP','PERIO1','PERIO5','PERIOSEPA','PROFSEPA','rsgsrgrs','sciosepa','sdfsdfsd','SDVSD','tar1','tar14','tar27','tar28','tarinv','tarJ','TODO1','TODO3','TODO4','UCAM2','uicdocentes','UICGRADO','uicsepa','UICSEPA3','dfgd','pet','ubonline');						
    public $codi_sessioPERIO = 'registrations_M20_';

    public $eventHIGIENE = 'mh20';
    public $cuotasHIGIENE = array('EXPOPP', 'inrex', 'intern', 'inv', 'invoro', 'invplat', 'KJHSDS', 'oralbfar', 'pac11', 'tar1', 'tar2', 'tar3', 'tar4', 'tar6', 'ssgref', 'pont', 'colval', 'tar5', 'tar7', 'TAR8', 'tar15', 'TAR10', 'TAR11', 'tar12', 'tar14', 'tar15', 'GRUP10', 'grUP15', 'GRUP20', 'grup101', 'grup102', 'tar104', 'grup105', 'grup17', 'grup109', 'grup1120', 'grup1103', 'grup1106', 'stall', 'COLHIGVA', 'Oral', '31ENERO', 'EXPOPP', 'de', 'HIGI1', 'HIGI2', 'HIGI3', 'HIGI4', 'HIGI5', 'HIGI6', 'HIGI7', 'HENRYH');
    public $codi_sessioHIGIENE = 'registrations_MH20_';

    public $eventGESTION = 'm20g';
    public $cuotasGESTION = array('agonge', 'AJA', 'ccxxc', 'dgred', 'INVITADOCOLEGIO', 'invt', 'sido', 'soc1', 'soc2', 'IVPAT', 'fdefgfd','GETIO','1CUOTA4','tar3546','TAR456','sfg3');
    public $codi_sessioGESTION = 'registrations_M20G_';

    public $eventINTER = 'm19i';
    public $cuotasINTER = array('INTERDIGISOCIO', 'SSD', 'TAR23', 'TAR4', 'tarifa', '249sepa', 'dfghjkjh', 'INTER1', 'INTER2', 'INTER4', 'INTER7');
    public $codi_sessioINTER = 'registrations_M19I_';


	public $eventDIGITAL = 'sd20';
	public $cuotasDIGITAL = array('soc1', 'soc2', 'so3', 'invt', 'tar345', 'TAAR43', 'tar3546', 'tar3546', 'TAR456', 'edu', '45fe', '45fwf', '32rfwe', '234e', 'ssdd', 'SVSVS', 'IVPAT', 'AJA', 'agonge', 'ddad', 'FGSBSF', 'FGSBSF', '31ENERO', 'EXPOPRR', 'EXPOPRT', 'INVITADOCOLEGIO', 'sfg3', 'dgred', 'dogddf', 'ccxxc', 'DIGI1', 'DIGI2', 'DIGI3', 'DIGI4', 'DIGI6' ,'DIGI7', 'DIGI8', 'DIGI9', 'DIGI10', 'HENRYD');
	public $codi_sessioDIGITAL = 'registrations_SD20_';


    function __construct()
    {

    }

    public function init()
    {
        if (empty($_POST["user"])){
            header('Location: /?error=No Data');
            exit;
        }

        $this->username = $_POST["user"];
        $this->eventCode = isset($_POST["eventCode"]) ? $_POST["eventCode"] : $this->eventCode;
        $this->member = new Member();

        setcookie($this->cookiename, base64_encode($this->eventCode), time()+3600);



        // Comprueba usuarios 'especiales' antes de comprobar usuarios softCongress
        $specialAccess = false;
        foreach($this->specialUsers as $user) {
            
            if ($this->username == $user ){
                $specialAccess = true;
                break;                
            } 
        }
        if($specialAccess === true){
            setcookie($this->cookieUsername, base64_encode(json_encode($this->username)), time()+3600);
            header('Location: ./view/dashboard-olis.php');
            exit;
        }

        $user = $this->member->validateUserCode($this->username);
        if(isset($user)) {
            header('Location: ./view/dashboard-olis.php');
            exit;
        }   
 
        header('Location: /?error=El codi no és correcte.');
        exit;
      

    }

    public function doLogin(){

        setcookie($this->cookieUsername, base64_encode(json_encode($this->user)), time()+3600);
        setcookie($this->cookiename, base64_encode($this->eventCode), time()+3600);
        //$this->registerVisit();//module not active

        header('Location: /view/hall.php');
        exit;
    }





    public function validateUserCongress($eventCode){

        $this->eventCode = $eventCode;
        $this->hashEvent();
        
        $routeURL = "validate/json/"; // Ruta a cridar, pot ser validate/json o sendpassword/json
        $this->url = $this->mainURL . $routeURL;

        $params = array(
            "congressCode" => $this->eventCode,
            "hashKey" => $this->hashKey,
            "username" => $this->username,
            "password" => $this->password
        );

        $verb = 'GET';

        $this->user = $this->rest_client($this->url, $params, $verb, $this->format);
        
        $validate = true;

        if(!$this->user) {
            $validate = false;
        }

        if ((string)$this->user->status == 'notfound') {
            $validate = false;
        } 

        return $validate;
    }


    public function hashEvent()
    {
        
        switch ($this->eventCode){
            //PERIO
            case $this->eventPERIO:
                $this->secretWord = "LLPPD1234Dee";
            break;
            //HIGIENE
            case $this->eventHIGIENE:
                $this->secretWord = "higiene23345";
            break;
            //GESTION
            case $this->eventGESTION:
                $this->secretWord = "9876ADFRED";
            break;
            //INTER
            case $this->eventINTER:
                $this->secretWord = "ppadfg1234";
            break;
			//DIGITAL
            case $this->eventDIGITAL:
                $this->secretWord = "cvbjhDFFF444";
            break;
        }

        $dateForHash = gmdate("Y-m-d");
        $this->hashKey = utf8_encode(sha1($this->secretWord . $this->eventCode . $dateForHash));
    }


    public function logout()
    {
        setcookie($this->cookieUsername, "", time()-3600);
        header('Location: /');
        exit;
    }

    public function getUser()
    {
        if(!isset($_COOKIE[$this->cookiename]) || !isset($_COOKIE[$this->cookieUsername]))
            header('Location: /');

        $this->eventCode = base64_decode($_COOKIE[$this->cookiename]);
        $this->hashEvent();
        $this->user = json_decode(base64_decode($_COOKIE[$this->cookieUsername]));
        
        return $this->user;
        //print_r($this->user);
    }

    public function showName()
    {

        echo sprintf("<h3>Bienvenido %s %s</h3>", $this->user->firstname, $this->user->surname);
    }

    public function showWorkshops()
    {
        $w = $this->details('Workshops');

        echo "<h3>Talleres Disponibles</h3>";
        echo "<ul>";

        foreach ($w->fields as $key => $field) {
            echo sprintf('<li>%s</li>', $field->caption);
        }
        echo "</ul>";
    }

	public function showDetails()
    {
        $w = $this->details('Registrations');

        echo "<h3>Details (Registrations)</h3>";		
        echo "<ul>";

        foreach ($w->fields as $key => $field) {
            echo sprintf('<li>%s : %s</li>', $field->feeCode, $field->caption);
        }
        echo "</ul>";
    }


    public function details($module = 'Registrations')
    {
        /**  Per veure quina inscripció té  */
        $routeURL = "details/json/";
        $this->url = $this->mainURL . $routeURL;

        $params = array("congressCode" => $this->eventCode,
            "hashKey" => $this->hashKey,
            "value" => $this->user->id,
            "type" => 'ID',
            "module" => $module
        );
        $verb = 'GET';


        return $this->rest_client($this->url, $params, $verb, $this->format);
    }


    public function getUserBasics(){

        if (empty($this->user->id)){
            // is specialUser!
            return array('',$this->user,'special user');
        }

        $routeURL = "details/json/";
        $this->url = $this->mainURL . $routeURL;

        $params = array("congressCode" => $this->eventCode,
            "hashKey" => $this->hashKey,
            "value" => $this->user->id,
            "type" => 'ID'
        );
        $verb = 'GET';

        $u = $this->rest_client($this->url, $params, $verb, $this->format);

      

        $userID = $u->fields->id->value;
        $userFullName = $u->fields->att_name->value ." ". $u->fields->att_surname1->value;
        $userEmail = $u->fields->att_email->value;

        return array($userID,$userFullName,$userEmail);
        
        /*
        echo "<h3>Store user data (name + email)</h3>";
        echo "userID:".$userID."<br>";
        echo "Name:".$userFullName."<br>";
        echo "Email:".$userEmail."<br>";

        echo "<h3>Store user data</h3>";
        echo "<pre>";
        print_r($u);
        echo "</pre>";
        */
    }
    

    public function getList()
    {

        /**  Retorna el llistat d'IDs o linkCode del "congres"  */
        $routeURL = "list/json/";
        $this->url = $this->mainURL . $routeURL;

        $params = array("congressCode" => $this->eventCode,
            "hashKey" => $this->hashKey,
            "type" => 'ID',
            "status" => 'All',
        );
        $verb = 'GET';

        $r = $this->rest_client($this->url, $params, $verb, $this->format);

        echo "<h3>List:</h3>";
        echo "<pre>";
        print_r($r);
        echo "</pre>";
    }


    public function personalData()
    {
        /**  Per veure les dades personals té  */
        $routeURL = "details/json/";
        $this->url = $this->mainURL . $routeURL;

        $params = array("congressCode" => $this->eventCode,
            "hashKey" => $this->hashKey,
            "value" => $this->user->id,
            "type" => 'ID'
        );
        $verb = 'GET';

        $r = $this->rest_client($this->url, $params, $verb, $this->format);

        
        echo "<h3>Personal data:</h3>";
        echo "<pre>";
        print_r($r);
        echo "</pre>";       

      
    }



	public function getFechaActual(){
		//$fechaActual = gmdate("d-M-Y");
		$fechaActual = "23-Ago-2021";
		return $fechaActual;
	}
	
	
	public function validateUserCuota($codigosCuota, $codi_sessio){

        $acceso = false;		
		$w = $this->details('Registrations');

		$codes = array();
        foreach ($w->fields as $key => $field) {
           if(!empty($field->feeCode)){
			   $codes[] = $field->feeCode;
		   };
        }
	
		if (count(array_intersect($codes, $codigosCuota)) === 0) {
		  // No values from array1 are in array 2
		} else {
		  // There is at least one value from array1 present in array2
		  $acceso = true;
		  $this->codi_sessio = $codi_sessio;
        }

		return $acceso;

    }
    

 
    /* !! Módulo desactivado en softcongress 

    public function registerVisit()
    {
        //  Per dir que acaba d'entrar al "congres"  
        $routeURL = "attendance/json/";
        $this->url = $this->mainURL . $routeURL;

        $params = array("congressCode" => $this->eventCode,
            "hashKey" => $this->hashKey,
            "id" => $this->user->id,
            "type" => 'ID',
            "controlType" => $this->codi_sessio,
            "actionToDo" => 'in'
        );
        $verb = 'POST';

        $r = $this->rest_client($this->url, $params, $verb, $this->format);

        //echo "<h3>attendance:</h3>";
        //echo "<pre>";
        //print_r($r);
        //echo "</pre>";
        //exit;
    }
    */


	public function loginTest(){
		$username ='Cgarcia1';
		$userpass = 'Cgarcia';
		$eventCODE = 'm20';
		$cuotasCODE = array('INVOR');
		$codi_sessioCODE = 'registrations_M20_';
		$secretWord = "LLPPD1234Dee";
		
		//Validate user ------------
		
		$this->eventCode = $eventCODE;
		$this->secretWord = $secretWord;
		$this->username = $username;
		$this->password = $userpass;
		
        $dateForHash = gmdate("Y-m-d");
        $this->hashKey = utf8_encode(sha1($this->secretWord . $this->eventCode . $dateForHash));
        
        $routeURL = "validate/json/"; // Ruta a cridar, pot ser validate/json o sendpassword/json
        $this->url = $this->mainURL . $routeURL;

        $params = array(
            "congressCode" => $this->eventCode,
            "hashKey" => $this->hashKey,
            "username" => $this->username,
            "password" => $this->password
        );

        $verb = 'GET';  
		
        return $this->rest_client($this->url, $params, $verb, $this->format);
		
		
		/**/
		
    
        /**  Per veure quina inscripció té  *//*
        $routeURL = "details/json/";
        $this->url = $this->mainURL . $routeURL;

        $params = array("congressCode" => $this->eventCode,
            "hashKey" => $this->hashKey,
            "value" => $this->user->id,
            "type" => 'ID',
            "module" => 'Registrations'
        );
        $verb = 'GET';


        return $this->rest_client($this->url, $params, $verb, $this->format);*/
		
		
	}




    private function rest_client($url, $params = null, $verb = 'GET', $format = 'json')
    {
        $cparams = array(
            'http' => array(
                'method' => $verb,
                'ignore_errors' => true
            )
        );
        if ($params !== null) {
            $params = http_build_query($params);
            if ($verb == 'POST') {
                $cparams['http']['content'] = $params;
            } else {
                $url .= '?' . $params;
            }
        }

        $context = stream_context_create($cparams);
        $fp = fopen($url, 'rb', false, $context);
        if (!$fp) {
            $res = false;
        } else {
            // If you're trying to troubleshoot problems, try uncommenting the
            // next two lines; it will show you the HTTP response headers across
            // all the redirects:
            // $meta = stream_get_meta_data($fp);
            // var_dump($meta['wrapper_data']);
            $res = stream_get_contents($fp);
        }

        if ($res === false) {
            throw new Exception("$verb $url failed: $php_errormsg");
            echo "$verb $url failed: $php_errormsg";
        }

        switch ($format) {
            case 'json':
                $r = json_decode($res);
                if ($r === null) {
                    //throw new Exception("failed to decode $res as json");
                    return false;
                }
                return $r;
            case 'xml':
                $r = simplexml_load_string($res);
                if ($r === null) {
                    return false;
                    throw new Exception("failed to decode $res as xml");
                }
                return $r;
        }
        return $res;
    }
}

/**
SEPA PERIO
CODIGO CONGRESO: m20
PALABRA SECRETA: LLPPD1234Dee
CODIGOS CUOTAS: 8, 1SIMP1, 1SIMP2, 249SEPA, 1SIMP2, 31ENERO, afw, agent, AGON, aneo, blackfriday, cddv, COLOM, CONGRESISTASEPA, dafavad, dcddc, DFGT54, DFSDA, dsfsd, esp249, EXPOP, FULLCON, g1, g13, g2, g38, g44, g47, gatu, infecte, INVOR, INVPL, KLOCKNR, LATAM, maxillaris, ORABLGR, PEP, PERIO1, PERIO5, PERIOSEPA, PROFSEPA, rsgsrgrs, sciosepa, sdfsdfsd, SDVSD, tar1, tar14, tar27, tar28, tarinv, tarJ, TODO1, TODO3, TODO4, UCAM2, uicdocentes, UICGRADO, uicsepa, UICSEPA3, dfgd

SEPA HIGIENE
CODIGO CONGRESO: mh20
PALABRA SECRETA: higiene23345
CODIGOS CUOTAS: EXPOPP, inrex, intern, inv, invoro, invplat,   KJHSDS, oralbfar, pac11, tar1, tar2, tar3, tar4, tar6, ssgref

SEPA GESTION
CODIGO CONGRESO: m20g
PALABRA SECRETA: 9876ADFRED
CODIGOS CUOTAS: agonge, AJA, ccxxc, dgred, INVITADOCOLEGIO, inv, sido, soc1, soc2

SEPA INTER
CODIGO CONGRESO: m19i
PALABRA SECRETA: ppadfg1234
CODIGOS CUOTAS: INTERDIGISOCIO, SSD, TAR23, TAR4, tarifa


*/



?>